## Installasi

Konfirgurasi file `.env`

untuk database
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```

untuk pusher

```
BROADCAST_DRIVER=pusher
PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=ap1
```

Jangan lupa lakukan printah artisan berikut

- `php artisan migrate`

Kemudian lakukan penginstalan packages

- `npm install` atau `yarn`

## Pemakaian

- `php artisan serve`
